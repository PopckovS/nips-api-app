from django.core.files.uploadedfile import SimpleUploadedFile
from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase

from api_img.models import Images
from api_img.serializers import ImagesSerializer

from django.conf import settings

import os


class ImagesApiTestCase(APITestCase):
    """
    Unit тесты для API url: api/v1/images
    """

    IMAGE_PATH = 'fixtures'
    PHOTO_IMAGE = 'photo.jpg'
    FILTER_IMAGE = 'filter.jpg'
    RESULT_IMAGE = 'result.jpg'

    def test_get_list(self):
        """GET: Тест на получения всех записей"""

        # print('path = ', os.path.join(os.curdir, self.IMAGE_PATH, self.PHOTO_IMAGE))

        img_photo = SimpleUploadedFile(name=self.PHOTO_IMAGE,
                                       # content=open(os.path.join(self.IMAGE_PATH, self.PHOTO_IMAGE), 'rb').read()
                                       )
        img_filter = SimpleUploadedFile(name=self.FILTER_IMAGE,
                                        # content=open(os.path.join(self.IMAGE_PATH, self.FILTER_IMAGE), 'rb').read()
                                        )
        img_result = SimpleUploadedFile(name=self.RESULT_IMAGE,
                                        # content=open(os.path.join(self.IMAGE_PATH, self.RESULT_IMAGE), 'rb').read()
                                        )

        # создание обьектов моделеи
        img_1 = Images.objects.create(type='photo',
                                      big_image=img_photo
                                      )
        img_2 = Images.objects.create(type='filter',
                                      big_image=img_filter
                                      )
        img_3 = Images.objects.create(type='result',
                                      big_image=img_result
                                      )

        # сериализация объектов
        serializer_data = ImagesSerializer([img_1, img_2, img_3], many=True).data

        # url на который должен отдавать все объекты
        url = reverse('images-list')

        # клиент делает запрос на url и получает результат всех записей
        response = self.client.get(url)

        # проверка на статус ответа от сервера
        self.assertEqual(status.HTTP_200_OK, response.status_code)
        # проверка на соответствие сериализуемых объектов друг другу
        self.assertEqual(serializer_data, response.data['results'])

    def test_get_one(self):
        """GET: Тест на получение одной конкретной записи"""
        # url на который должен отдавать один обьект
        url = reverse('images-detail', kwargs={'pk': 2})

        # создание обьектов моделеи
        img_1 = Images.objects.create(type='photo')
        img_2 = Images.objects.create(type='filter')

        # сериализация обьектов модели, и получение только одного обьекта
        serializer_data = ImagesSerializer([img_1, img_2], many=True).data
        one_serializer_data = serializer_data[1]

        # клиент делает запрос на url и получает результат одной запии
        response = self.client.get(url)

        # проверка статуса ответа
        self.assertEqual(status.HTTP_200_OK, response.status_code)
        # проверка на соответствие сериализуемых объектов друг другу
        self.assertEqual(one_serializer_data, response.data)

    def test_delete_one(self):
        """DELETE: Тест на удаление одной конкретной записи"""

        # url для удаления конкретного обьекта
        url = reverse('images-detail', kwargs={'pk': 3})

        # создание обьектов моделеи
        img_1 = Images.objects.create(type='photo')
        img_2 = Images.objects.create(type='filter')
        img_3 = Images.objects.create(type='result')

        # проверка обьектов на количество
        self.assertEqual(Images.objects.count(), 3)

        # удаление обьекта по точке API
        response = self.client.delete(url)

        # проверка статуса ответа
        self.assertEqual(status.HTTP_204_NO_CONTENT, response.status_code)
        # проверка обьектов на количество
        self.assertEqual(Images.objects.count(), 2)





