from api_img.serializers import ImagesSerializer
from api_img.models import Images
from django.test import TestCase


class ImagesSerializersTesCase(TestCase):
    """Тесты для проверки сериализатора"""

    def test_ok(self):
        """Тест на проверку сериализуемых объектов модели Images"""

        # создание объектов в модели
        img_1 = Images.objects.create(type='photo')
        img_2 = Images.objects.create(type='filter')
        img_3 = Images.objects.create(type='result')

        # специализированные реальные объекты
        real_data = ImagesSerializer([img_1, img_2, img_3], many=True).data

        # ожидаемые специализированные объекты
        expected_data = [
            {'id': img_1.id, 'type': 'photo',  'big_image': None, 'min_image': None, 'parent_photo': None, 'parent_filter': None},
            {'id': img_2.id, 'type': 'filter', 'big_image': None, 'min_image': None, 'parent_photo': None, 'parent_filter': None},
            {'id': img_3.id, 'type': 'result', 'big_image': None, 'min_image': None, 'parent_photo': None, 'parent_filter': None},
        ]

        # проверка сексуализированных моделей на соответствие
        self.assertEqual(expected_data, real_data)




